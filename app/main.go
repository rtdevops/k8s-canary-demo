package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

func cpuIntensive(p *int64) {
	for i := int64(1); i <= 10000000; i++ {
		*p = i
	}
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		x := int64(0)
		go cpuIntensive(&x)
		fmt.Fprintf(w, "Hello! I'm %s.", strings.Join(os.Args[1:], " "))
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
