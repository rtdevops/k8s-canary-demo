# k8s-canary-demo
A quick example of how to use canary deployments and autoscaling in Kubernetes.

This inteded to be run on localhost for demonstration purposes only.

Tested on minikube v1.6.2 on Darwin 10.14.6 (Kubernetes v1.17.0 on Docker '19.03.5').

```
# Start minikube
minikube start

# Enable metrics-server (required for hpa)
minikube addons enable metrics-server

# Check that metrics-server works (may take 1-2 mins)
kubectl top node

# Deploy app v1 (3 replicas by default)
kubectl apply -f kube-app-v1.yml

# Deploy app v2 (1 replica by default)
kubectl apply -f kube-app-v2.yml

# Check that both versions respond and generate workload
URL="$(minikube service demo-app --url)"; while sleep 0.1; do curl -w '\n' "${URL}"; done

# Looking at autoscaling (may take 30-60 seconds in minikube)
watch -n 3 kubectl get deployments

# If we stop workload, the number of running pods will be scaled down after some time.
```