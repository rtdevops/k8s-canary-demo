FROM golang:1.13 as builder
COPY ./app/ /usr/src/app
WORKDIR /usr/src/app
RUN CGO_ENABLED=0 GOOS="linux" go build -ldflags "-s -w" -o demo-app *.go
RUN chmod +x demo-app

FROM scratch
LABEL maintainer="Roman Tishchenko / me@outofservice.io"
COPY --from=builder /usr/src/app/demo-app /
ENTRYPOINT ["/demo-app"]